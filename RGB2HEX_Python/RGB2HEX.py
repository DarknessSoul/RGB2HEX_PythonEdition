from colormap import rgb2hex
import os
def RGB2HEX(r, g, b):
    return rgb2hex(r, g, b)

if __name__ == "__main__":
    red = input("Write Red Value: ")
    blue = input("Write Green Value: ")
    gr_input = input("Write Blue Value: ")
    with open(os.getcwd() + "\\Input.txt", "w") as x:
        x.write(RGB2HEX(int(red), int(gr_input), int(blue)))
